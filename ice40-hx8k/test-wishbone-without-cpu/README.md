# Test desde Whisbone a través de CRS

La placa no cuenta con un botón de usuario para
hacer reset del reloj; para la prueba, puede dejar el
pin reset asociado a un led y además cambiar la lógica de
del pin ejemplo:
```python

# ~platform.request("clk_reset", 0))
platform.request("user_led", 0))
```
## Instrucciones para hacer el test

1. make gateware
2. make flash
3. Ejecutar en una consola el servidor de litex: `litex_server --uart --uart-port /dev/ttyUSB1`
4. python test/leds.py

Saludos:
Johnny Cubides
