#!/usr/bin/env python3

from litex.build.generic_platform import IOStandard, Subsignal, Pins
from migen.build.generic_platform import Misc
from migen.genlib.io import CRG
from migen.fhdl.structure import Cat
from litex.build.lattice import LatticePlatform
from litex.soc.integration.soc_core import SoCMini
from litex.soc.cores.uart import UARTWishboneBridge
from ios import Led
from litex.soc.integration.builder import Builder

# IOs ------------------------------------------------------------------------
_io = [
    ("user_led", 0, Pins("B5"), IOStandard("LVCMOS33")),
    ("user_led", 1, Pins("B4"), IOStandard("LVCMOS33")),
    ("user_led", 2, Pins("A2"), IOStandard("LVCMOS33")),
    ("user_led", 3, Pins("A1"), IOStandard("LVCMOS33")),
    ("user_led", 4, Pins("C5"), IOStandard("LVCMOS33")),
    ("user_led", 5, Pins("C4"), IOStandard("LVCMOS33")),
    ("user_led", 6, Pins("B3"), IOStandard("LVCMOS33")),
    ("user_led", 7, Pins("C3"), IOStandard("LVCMOS33")),

    ("serial", 0,
        Subsignal("rx", Pins("B10")),
        Subsignal("tx", Pins("B12"), Misc("PULLUP")),
        Subsignal("rts", Pins("B13"), Misc("PULLUP")),
        Subsignal("cts", Pins("A15"), Misc("PULLUP")),
        Subsignal("dtr", Pins("A16"), Misc("PULLUP")),
        Subsignal("dsr", Pins("B14"), Misc("PULLUP")),
        Subsignal("dcd", Pins("B15"), Misc("PULLUP")),
        IOStandard("LVCMOS33"),),

    ("spiflash", 0,
        Subsignal("cs_n", Pins("R12"), IOStandard("LVCMOS33")),
        Subsignal("clk", Pins("R11"), IOStandard("LVCMOS33")),
        Subsignal("mosi", Pins("P12"), IOStandard("LVCMOS33")),
        Subsignal("miso", Pins("P11"), IOStandard("LVCMOS33")),),

    ("clk12", 0, Pins("J3"), IOStandard("LVCMOS33"))
]


class Platform(LatticePlatform):
    default_clk_name = "clk12"
    default_clk_period = 88.333

    gateware_size = 0x28000

    spiflash_model = "n25q32"
    spiflash_read_dummy_bits = 8
    spiflash_clock_div = 2
    spiflash_total_size = int((32/8)*1024*1024)  # 32Mbit
    spiflash_page_size = 256
    spiflash_sector_size = 0x10000

    def __init__(self):
        LatticePlatform.__init__(
            self,
            device="ice40-hx8k-ct256",
            io=_io,
            # connectors=_connectors???
            toolchain="icestorm")


platform = Platform()


class BaseSoC(SoCMini):
    def __init__(self, platform, **kwargs):
        sys_clk_freq = int(12e6)
        # clk_freq=25e6,
        # SoCMini ------------------------------------------------------------
        SoCMini.__init__(
            self, platform,
            sys_clk_freq,
            csr_data_width=32,
            ident="My first LiteX System On Chip",
            ident_version=True)

        # Clock Reset Generation ---------------------------------------------
        self.submodules.crg = CRG(
            platform.request("clk12"),
            ~platform.request("user_led", 7))

        # LEDs Wishbone-uart bridge-------------------------------------------
        self.submodules.serial_bridge = UARTWishboneBridge(
            platform.request("serial", 0),
            sys_clk_freq)
        self.add_wb_master(self.serial_bridge.wishbone)

        # LEDs ---------------------------------------------------------------
        user_leds = Cat(*[platform.request("user_led", i) for i in range(2)])
        self.submodules.leds = Led(user_leds)
        self.add_csr("leds")


soc = BaseSoC(platform)
builder = Builder(soc, output_dir="build", csr_csv="test/csr.csv")
builder.build()
